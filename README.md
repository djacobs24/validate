# Validate
Validate is used to help with the boring stuff - validating inputs. It is 
extremely well tested with several foundational [`Validator`](validators.go)s 
available out of the box, but it is also meant to be extended on with custom 
validators.

## Usage
```go
type Request struct {
    Name     string `json:"name"`
    Username string `json:"username"`
    Age      int    `json:"age"`
    ...
}

func (r *Request) Validate() error {
    return validate.Validate(
        validate.StringProvided("Name", r.Name).WithCustomError(errors.New("you must provide a name")),
        validate.StringLenLessThan("Username", r.Username, 30).IfStringProvided(&r.Username),
        validate.IntGreaterThan("Age", r.Age, 17),
        ...
    )
}
```

## Defining Custom Validators
A [`Validator`](validators.go) is simply a function that takes no arguments and 
returns an error which means making custom validators is as easy as:

```go
// StringStartsWith validates a string starts with a particular substring.
func StringStartsWith(fieldName, fieldValue, suffix string) Validator {
    return func() error {
        if !strings.HasSuffix(fieldValue, suffix) {
            return fmt.Errorf("%s must start with %q", fieldName, suffix)
        }
        
        return nil
    }
}
```

Then simply pass your newly created `Validator` to the [`Validate`](validate.go) 
function like so:
```go
if err := validate.Validate(
    validate.StringStartsWith("Name", req.Name, "D"),
); err != nil {
	return err
}
```

You can even use the validator [_extensions_](extensions.go) on your newly 
created `Validator` like so:
```go
if err := validate.Validate(
    validate.StringStartsWith("Name", req.Name, "D").IfStringProvided(&req.Name),
); err != nil {
	return err
}
```
