package validate

// IfStringProvided only executes the associated validator if the string is provided.
func (v Validator) IfStringProvided(value *string) Validator {
	return func() error {
		if value == nil {
			return nil
		}

		return v()
	}
}

// IfBoolProvided only executes the associated validator if the bool is provided.
func (v Validator) IfBoolProvided(value *bool) Validator {
	return func() error {
		if value == nil {
			return nil
		}

		return v()
	}
}

// IfIntProvided only executes the associated validator if the int is provided.
func (v Validator) IfIntProvided(value *int) Validator {
	return func() error {
		if value == nil {
			return nil
		}

		return v()
	}
}

// WithCustomError provides a way to specify custom error messages for validators.
func (v Validator) WithCustomError(customErr error) Validator {
	return func() error {
		if err := v(); err != nil {
			return customErr
		}

		return nil
	}
}
