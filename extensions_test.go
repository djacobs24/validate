package validate_test

import (
	"errors"
	"testing"

	"gitlab.com/djacobs24/validate"
)

func TestValidator_IfStringProvided(t *testing.T) {
	var (
		provided    = "David"
		notProvided *string
	)
	type args struct {
		value *string
	}
	tests := []struct {
		name string
		v    validate.Validator
		args args
		want error
	}{
		{
			name: "string provided returns error",
			v:    func() error { return errors.New("this validator errors") },
			args: args{
				value: &provided,
			},
			want: errors.New("this validator errors"),
		},
		{
			name: "string not provided returns nil",
			v:    func() error { return errors.New("this validator errors") },
			args: args{
				value: notProvided,
			},
			want: nil,
		},
		{
			name: "string not provided returns nil",
			v:    func() error { return errors.New("this validator errors") },
			args: args{},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := tt.v.IfStringProvided(
				tt.args.value,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestValidator_IfBoolProvided(t *testing.T) {
	var (
		provided    = false
		notProvided *bool
	)
	type args struct {
		value *bool
	}
	tests := []struct {
		name string
		v    validate.Validator
		args args
		want error
	}{
		{
			name: "bool provided returns error",
			v:    func() error { return errors.New("this validator errors") },
			args: args{
				value: &provided,
			},
			want: errors.New("this validator errors"),
		},
		{
			name: "bool not provided returns nil",
			v:    func() error { return errors.New("this validator errors") },
			args: args{
				value: notProvided,
			},
			want: nil,
		},
		{
			name: "bool not provided returns nil",
			v:    func() error { return errors.New("this validator errors") },
			args: args{},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := tt.v.IfBoolProvided(
				tt.args.value,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestValidator_IfIntProvided(t *testing.T) {
	var (
		provided    = 1
		notProvided *int
	)
	type args struct {
		value *int
	}
	tests := []struct {
		name string
		v    validate.Validator
		args args
		want error
	}{
		{
			name: "int provided returns error",
			v:    func() error { return errors.New("this validator errors") },
			args: args{
				value: &provided,
			},
			want: errors.New("this validator errors"),
		},
		{
			name: "int not provided returns nil",
			v:    func() error { return errors.New("this validator errors") },
			args: args{
				value: notProvided,
			},
			want: nil,
		},
		{
			name: "int not provided returns nil",
			v:    func() error { return errors.New("this validator errors") },
			args: args{},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := tt.v.IfIntProvided(
				tt.args.value,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestValidator_WithCustomError(t *testing.T) {
	type args struct {
		customErr error
	}
	tests := []struct {
		name string
		v    validate.Validator
		args args
		want error
	}{
		{
			name: "validator errors, return extensions error",
			v:    func() error { return errors.New("the validator error") },
			args: args{
				customErr: errors.New("the extension error"),
			},
			want: errors.New("the extension error"),
		},
		{
			name: "validator does not error, return nil",
			v:    func() error { return nil },
			args: args{
				customErr: errors.New("the extension error"),
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			validator := tt.v.WithCustomError(
				tt.args.customErr,
			)
			if err := compareValidatorError(validator, tt.want); err != nil {
				t.Error(err)
			}
		})
	}
}
