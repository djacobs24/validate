package validate_test

import (
	"errors"
	"testing"

	"gitlab.com/djacobs24/validate"
)

func TestValidate(t *testing.T) {
	type args struct {
		validators []validate.Validator
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "returns the first error",
			args: args{[]validate.Validator{
				func() error {
					return errors.New("first error")
				}, func() error {
					return errors.New("second error")
				}, func() error {
					return errors.New("third error")
				},
			}},
			want: errors.New("first error"),
		},
		{
			name: "returns no error",
			args: args{[]validate.Validator{
				func() error {
					return nil
				}, func() error {
					return nil
				}, func() error {
					return nil
				}, func() error {
					return nil
				},
			}},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := validate.Validate(tt.args.validators...)
			if err == nil && tt.want == nil {
				return
			}

			if err != nil && tt.want == nil {
				t.Errorf("Validate() err = %v, want nil", err)
			}

			if err == nil && tt.want != nil {
				t.Errorf("Validate() err = nil, want %v", tt.want)
			}

			if err.Error() != tt.want.Error() {
				t.Errorf("Validate() err = %v, want %v", err, tt.want)
			}
		})
	}
}
