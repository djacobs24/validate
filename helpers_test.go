package validate

import "testing"

func Test_replaceNth(t *testing.T) {
	type args struct {
		str string
		old string
		new string
		n   int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "replace the second occurrence",
			args: args{
				str: "oneonethreefourfive",
				old: "one",
				new: "two",
				n:   2,
			},
			want: "onetwothreefourfive",
		},
		{
			name: "nothing to replace",
			args: args{
				str: "oneonethreefourfive",
				old: "old",
				new: "new",
				n:   1,
			},
			want: "oneonethreefourfive",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := replaceNth(tt.args.str, tt.args.old, tt.args.new, tt.args.n); got != tt.want {
				t.Errorf("replaceNth() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getFieldNamesWithNumValues(t *testing.T) {
	type args struct {
		fieldNamesAndValues []string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		want1   int
		wantErr bool
	}{
		{
			name:    "three fields with values",
			args:    args{fieldNamesAndValues: []string{"Username", "david", "Password", "password", "Email", "david@example.com"}},
			want:    "Username, Password or Email",
			want1:   3,
			wantErr: false,
		},
		{
			name:    "two fields with values",
			args:    args{fieldNamesAndValues: []string{"Username", "david", "Password", "password"}},
			want:    "Username or Password",
			want1:   2,
			wantErr: false,
		},
		{
			name:    "one field with value",
			args:    args{fieldNamesAndValues: []string{"Username", "david"}},
			want:    "Username",
			want1:   1,
			wantErr: false,
		},
		{
			name:    "zero fields with value",
			args:    args{fieldNamesAndValues: []string{}},
			want:    "",
			want1:   0,
			wantErr: false,
		},
		{
			name:    "three fields, two values",
			args:    args{fieldNamesAndValues: []string{"Username", "david", "Password", "", "Email", "david@example.com"}},
			want:    "Username, Password or Email",
			want1:   2,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := getFieldNamesWithNumValues(tt.args.fieldNamesAndValues)
			if (err != nil) != tt.wantErr {
				t.Errorf("getFieldNamesWithNumValues() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getFieldNamesWithNumValues() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("getFieldNamesWithNumValues() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
