package validate

// Validate executes the Validator(s) passed in.
// It returns the first error that it encounters.
func Validate(validators ...Validator) error {
	for _, validator := range validators {
		if err := validator(); err != nil {
			return err
		}
	}

	return nil
}
