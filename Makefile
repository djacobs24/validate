.PHONY: test
test:
	go test -v -cover ./...

.PHONY: lint
lint:
	golangci-lint run